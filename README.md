Potrebna verzija PHP-a je vjerojatno 5.5+ (nisam 100% siguran, ali na njoj sam radio pa na tome sigurno radi :))

Potrebno je imati i yii (verzija 1.1.14) može se skinuti ovdje: [yii 1.1.14](https://www.dropbox.com/s/itnesnzavtqgc6a/yii1114.rar)

Također skripta za automatsko ubacivanje stranica se nalazi: [Skripta](https://www.dropbox.com/s/9yqv9u8z84lv8bd/Skripte.rar)
Može se pokrenuti iz cygwina samo sa ./zavrsni.sh
Postoje već neki rezultati koje sam ja testirao (prvih 2000 iz top domena) tražilica je pronađena u OK.txt, a u GRESKA.txt se nalaze one za koje nisam uspio pronaći tražilicu
iz OK nisam uspio kloniti sve koje mogu pretraživati, zato što je teško otkriti javascript

Također potrebno je importat mysql bazu  (ona se nalazi pod protected\data) i postaviti postavke u main.php

za wamp struktura foldera bi isla otprilike ovako

* WAMP
* * WWW
* * * zavrsni
* * * * index.php
* * * * protected
* * * * itd.

* * yii1114
* * * raspakirani zip