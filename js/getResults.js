var za_proci = [];
var trenutno = [];
var gotovo = [];
var i = 0;

idiDalje();

var idiDalje = function(){
	if(trenutno.length > 3){
		setTimeout(idiDalje, 1000);
	} else {
		if(za_proci.length > 0){
			for(var a in za_proci){
				trenutno[a] = za_proci[a];
				getData(a, za_proci[a]);
				delete za_proci[a];
				break;
			}
		}
	}
	
	if(i>3){
		refreshGrid();
		i = 0;
	}
	
}

var getData = function(idQuery, idSite) {
	var status = '';
	$('#querysite' + idSite).html('OBRAĐUJEM');
	$.ajax({
		url: '/zavrsni/index.php/query/getLinks/' + idQuery,
		type: 'GET',
		dataType: 'json',
		success: function()
		{
			status = 'GOTOVO';
		},
		error: function(){
			status = '';
		}
	}).done(function(result){
		$('#querysite' + idSite).html(status);
		gotovo[idQuery] = trenutno[idQuery];
		delete trenutno[idQuery];
		i++;
	});
};

var init = function(){
	for(var a in za_proci){
		$('#querysite' + idSite).html('Čekam..');
	}
	for(var a in gotovo){
		$('#querysite' + idSite).html('GOTOVO');
	}
}

var refreshGrid = function(){
		$.fn.yiiGridView.update('query-result-grid');
}