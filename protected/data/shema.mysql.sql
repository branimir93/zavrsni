-- phpMyAdmin SQL Dump
-- version 4.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2014 at 08:09 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zavrsni`
--
CREATE DATABASE IF NOT EXISTS `zavrsni2` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `zavrsni2`;

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

DROP TABLE IF EXISTS `keyword`;
CREATE TABLE IF NOT EXISTS `keyword` (
`id` int(11) NOT NULL,
  `text` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `query`
--

DROP TABLE IF EXISTS `query`;
CREATE TABLE IF NOT EXISTS `query` (
`id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keyword_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=93 ;

-- --------------------------------------------------------

--
-- Table structure for table `query_result`
--

DROP TABLE IF EXISTS `query_result`;
CREATE TABLE IF NOT EXISTS `query_result` (
`id` int(11) NOT NULL,
  `query_site_id` int(11) DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12432 ;

-- --------------------------------------------------------

--
-- Table structure for table `query_site`
--

DROP TABLE IF EXISTS `query_site`;
CREATE TABLE IF NOT EXISTS `query_site` (
`id` int(11) NOT NULL,
  `query_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `query_start` datetime DEFAULT NULL,
  `query_finish` datetime DEFAULT NULL,
  `query_result_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7957 ;

-- --------------------------------------------------------

--
-- Table structure for table `query_site_status`
--

DROP TABLE IF EXISTS `query_site_status`;
CREATE TABLE IF NOT EXISTS `query_site_status` (
`id` int(11) NOT NULL,
  `text` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
CREATE TABLE IF NOT EXISTS `site` (
`id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `url_template` text COLLATE utf8_unicode_ci,
  `form_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4939 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_post`
--

DROP TABLE IF EXISTS `site_post`;
CREATE TABLE IF NOT EXISTS `site_post` (
`id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `post_fields` text COLLATE utf8_unicode_ci,
  `boundary` text COLLATE utf8_unicode_ci COMMENT 'Koristimo za multipart encoding'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keyword`
--
ALTER TABLE `keyword`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `text` (`text`);

--
-- Indexes for table `query`
--
ALTER TABLE `query`
 ADD PRIMARY KEY (`id`), ADD KEY `query_keyword_id_fk_idx` (`keyword_id`);

--
-- Indexes for table `query_result`
--
ALTER TABLE `query_result`
 ADD PRIMARY KEY (`id`), ADD KEY `query_result_query_site_id_idx` (`query_site_id`);

--
-- Indexes for table `query_site`
--
ALTER TABLE `query_site`
 ADD PRIMARY KEY (`id`), ADD KEY `query_sites_query_id_fk_idx` (`query_id`), ADD KEY `query_site_site_id_fk_idx` (`site_id`), ADD KEY `status` (`status_id`);

--
-- Indexes for table `query_site_status`
--
ALTER TABLE `query_site_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_post`
--
ALTER TABLE `site_post`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `site_id_2` (`site_id`), ADD KEY `site_id` (`site_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keyword`
--
ALTER TABLE `keyword`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `query`
--
ALTER TABLE `query`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `query_result`
--
ALTER TABLE `query_result`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12432;
--
-- AUTO_INCREMENT for table `query_site`
--
ALTER TABLE `query_site`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7957;
--
-- AUTO_INCREMENT for table `query_site_status`
--
ALTER TABLE `query_site_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4939;
--
-- AUTO_INCREMENT for table `site_post`
--
ALTER TABLE `site_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `query`
--
ALTER TABLE `query`
ADD CONSTRAINT `query_keyword_id_fk` FOREIGN KEY (`keyword_id`) REFERENCES `keyword` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `query_result`
--
ALTER TABLE `query_result`
ADD CONSTRAINT `query_result_query_site_id_fk` FOREIGN KEY (`query_site_id`) REFERENCES `query_site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `query_site`
--
ALTER TABLE `query_site`
ADD CONSTRAINT `query_site_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `query_site_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `query_site_query_id_fk` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `query_site_site_id_fk` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `site_post`
--
ALTER TABLE `site_post`
ADD CONSTRAINT `site_post_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO `query_site_status` (`id`, `text`) VALUES
(1, 'ČEKAM'),
(2, 'OBRAĐUJEM'),
(3, 'GOTOVO'),
(4, 'ERROR');