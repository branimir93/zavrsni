<?php
Yii::import('zii.widgets.grid.CGridColumn');

class SitesColumn extends CDataColumn
{
    /**
    * @param integer $row the row number (zero-based)
    * @param Site[] $data the data associated with the row
    */
    public function renderDataCellContent($row, $data) // $row number is ignored
    { 
        $skupi = array_map(
            function($site) 
            {
                return CHtml::link($site->title, $site->url);
            }, $data->sites);
        echo implode(', ', array_slice($skupi, 0, 10));
    }
}