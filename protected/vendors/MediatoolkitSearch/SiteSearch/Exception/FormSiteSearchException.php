<?php
	
namespace MediatoolkitSearch\SiteSearch\Exception;

use MediatoolkitSearch\Exception\SiteSearchException;

/**
 * Description of FormSiteSearchException
 *
 * @author Branimir
 */
class FormSiteSearchException extends SiteSearchException {

}
	