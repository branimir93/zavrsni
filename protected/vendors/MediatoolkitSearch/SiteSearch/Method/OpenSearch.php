<?php
namespace MediatoolkitSearch\SiteSearch\Method;

use MediatoolkitSearch\Common\Functions;
use MediatoolkitSearch\SiteSearch\Interfaces\IRequest;
use MediatoolkitSearch\Exception\XMLFormatNotValidException;
use MediatoolkitSearch\Common\URL;

/**
 * Description of OpenSearch
 *
 * @author Branimir
 */
class OpenSearch implements IRequest {
        
	/**
	 *
	 * @var string Regex korišten za pretraživanje da li postoji 
	 * <link ... type="application/opensearchdescription\+xml" ...> u HTML-u
	 */
	public static $regex = '#<link(\s+|\s+[^>]*\s+)type=(["\'])application/opensearchdescription\+xml\2[^>]*>#siUu';

	/**
	 *
	 * @var \SiteSearch\Site Stranica nad kojom se pretražuje
	 */
	private $site;

	/**
	 *
	 * @var string URL template koji dobijemo iz XML-a
	 */
	private $URL;
	
	private $type = 10;

	public function __construct($site) {
		$this->site = $site;
		$this->setData();
	}

	public function getHeader(){
	}

	/**
	 * 
	 * @return URL
	 */
	public function getURL(){
		return $this->URL;
	}

	/**
	 * Pronalazi link na XML datoteku
	 * ukoliko ne postoji 
	 * <link ... type="application/opensearchdescription\+xml" ...> unutar 
	 * koda vraća prazan niz
	 * 
	 * @return string
	 */
	private function findLink(){
		if(preg_match(self::$regex, $this->site->getHTML(), $linkTag)){
			preg_match('#href\s*=\s*(["\'])([^\1]+)\1#siUum', $linkTag[0], $matches);
			return $matches[2];
		}
		else {
			return '';
		}
	}

	private function setData(){
		$url = $this->findLink();
		if(!empty($url)){
			libxml_use_internal_errors(true);
			$xml = simplexml_load_string(
					$this->fetchXML($url), 'SimpleXMLElement'
				);
			
			if (!$xml) {
				libxml_clear_errors();
				throw new XMLFormatNotValidException('XML is not valid');
			}

			$searchUrl = $this->getURLFromXML($xml);
			
			if($this->tryUrl($searchUrl)){
				$this->URL = new URL($searchUrl);
			}
			else {
				throw new XMLFormatNotValidException('Url u XML-u nije dobar ili se rezultati ucitavaju javascriptom');
			}
		}
		else {
			throw new XMLFormatNotValidException('HTML ne sadrži potrebne oznake za pretragu pomoću OpenSearcha.');
		}
	}
	
	private function tryUrl($url){
		$keyword = 'OVONEPOSTOJIDEF';
		$this->site->setUrl(str_ireplace('{searchTerms}', $keyword, $url));
		
		$ret = false;
		if(stripos($this->site->getHTML(), $keyword)){
			$ret = true;
		}
		return $ret;
	}

	/**
	 * Dohvaća zadanu XML datoteku sa zadanog URL-a te ju vraća kao string 
	 * 
	 * @param string $url
	 * @return string
	 */
	private function fetchXML($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, Functions::rel2abs($url, $this->site->getUrl()));

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		return $data;
	}

	/**
	 * Dohvaća URL template tag iz XML-a
	 * 
	 * @param SimpleXMLElement $xml XML dokument sa podacima o tražilici na 
	 * stranici
	 * @throws FormatNotValidException Ako format XML-a nije valjan te ako 
	 * unutar njega nije sadržan URL sa template atributom
	 */
	private function getURLFromXML($xml){
		if($xml){
			foreach($xml->Url as $url){
				if('' != (string) $url['template'] && (string) 'text/html' == $url['type']){
					return preg_replace(array('/\{([^\}\*\?]+)[\*\?]?\}[\*\?]?/'), '{$1}', (string)$url['template']);
				}
			}
		}

		throw new XMLFormatNotValidException("XML format datoteke je krivi.");
	}
	
	public function getFormType(){
		return 'GET';
	}
	
	public function getPostFields(){
		return '';
	}
	
	public function getBoundary(){
		return '';
	}
	
	public function getType(){
		return $this->type;
	}
}