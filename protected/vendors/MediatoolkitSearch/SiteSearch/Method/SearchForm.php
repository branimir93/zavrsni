<?php
namespace MediatoolkitSearch\SiteSearch\Method;

use MediatoolkitSearch\SiteSearch\Interfaces\IAnalyze;
use MediatoolkitSearch\SiteSearch\Interfaces\IRequest;
use DOMDocument;
use MediatoolkitSearch\SiteSearch\Form\Form;
use MediatoolkitSearch\SiteSearch\Form\FormField;
use MediatoolkitSearch\Common\Site;
use MediatoolkitSearch\Common\URL;
use MediatoolkitSearch\Common\Header;

use MediatoolkitSearch\SiteSearch\Exception\FormSiteSearchException;

	/**
 * Description of SearchForm
 *
 * @author Branimir
 * 
 */
class SearchForm implements IRequest {
    
	// <editor-fold desc="Variables">

	/**
	*
	* @var Site
	*/
	private $site;
	
	/**
	 *
	 * @var URL
	 */
	private $URL;
	
	/**
	 *
	 * @var Header
	 */
	private $header;
	
	private $formType;
	
	private $type = 20;
	
	private $postFields = '';
	
	/**
	 *
	 * @var DOMDocument
	 */
	private $dom;
	
	private $baseUrl;
	
	/**
	 *
	 * @var Form
	 */
	private $form;
	
	/**
	 *
	 * @var string
	 */
	private $boundary;
	
	// </editor-fold>
	
	// <editor-fold desc="GETTERS AND SETTERS">
	
	public function getHeader(){
		return $this->header;
	}

	public function getURL(){
		return $this->URL;
	}
	
	public function getFormType(){
		return $this->formType;
	}
	
	public function getPostFields(){
		return $this->postFields;
	}
	
	public function getBoundary(){
		return $this->boundary;
	}
	
	public function getType(){
		return $this->type;
	}
	
	// </editor-fold>
	
	// <editor-fold desc="PUBLIC FUNCTIONS">
	
	/**
	 * Stvara se novo DOM stablo nad zadanom stranicom
	 * 
	 * @param Site $site Stranica nad kojom se obavlja pretraga
	 */
	public function __construct($site) {
		$this->baseUrl = $site->getUrl();
		
		$this->site = $site;
		$this->dom = new DOMDocument();
		@$this->dom->loadHTML($this->site->getHTML());
		$this->setData();
	}
	// </editor-fold>
	
	// <editor-fold desc="PRIVATE FUNCTIONS">
	
	private function setData(){
		$url = $this->site->getUrl();
		foreach($this->regexForm() as $DOMform){
			$f = new Form();
			$f->loadByDOM($DOMform, $url);
						
			if($this->getQueryUrl($f)){
				$this->form = $f;
				break;
			}
		}
	}
	
	private function getQueryUrl(Form $form){
		$possible = $form->getFormData()->possibleQueryFields();
		$ret = false;
		foreach ($possible->toArray() as $key => $field){
			try {
				$found = $this->tryForm($form, $key);
			} catch (FormSiteSearchException $ex) {
				$found=false;
				//Logiraj
			}
			if($found){
				$ret = true;
				break;
			}
		}
		return $ret;
	}
	
	private function tryForm(Form $form, $inputName){
		$keyword = 'OVONEPOSTOJIDEF';
		$valueBackup = $form->getFormData()->getInputValue($inputName);	
		$form->getFormData()->changeValue($inputName, $keyword);
		$this->submit($form);
		
		$ret = false;
		if(stripos($this->site->getHTML(), $keyword)){
			$this->URL = new URL(
					str_ireplace($keyword, '{searchTerms}', $this->site->getUrl())
			);
			$this->type = 21;
			
			//Ako je doslo do redirekcije biti ce zapisano tu... i vjerojatno samo ako je boundary treba zapisat..
			$this->header = $this->site->getLastHeader();
			
			$this->formType = substr($this->header, 0, strpos($this->header, ' '));
			
			//jednako je ako postoji slucajno ista rijec kao keyword
			// vjerojatnost == 0, al et da se nadje :)
			if($this->formType == 'POST'){
				if($form->getEnctype() == 'application/x-www-form-urlencoded'){
					$this->type = 22;
					$this->postFields = str_ireplace("=$keyword", '={searchTerms}', $this->postFields);
					$this->boundary = '';
				}
				else {
					$this->type = 23;
					$this->postFields = str_replace("\n\n$keyword", "\n\n{searchTerms}", $this->postFields);
				}
			}
			
			$ret = true;
		}
		else {
			/**
			 * OVDJE JE KLJUC ZA JAVASCRIPT
			 * 
			 * Nesto sa imenom domene izmislit ako nema .. vjerojatno javascript
			 */
		}
		
		$form->getFormData()->changeValue($inputName, $valueBackup);
		return $ret;
	}
	
	/**
	 * Pretražuje forme te vraća one koje se mogu iskoristiti
	 * @return Form[]
	 */
	private function regexForm(){
		$forms = $this->dom->getElementsByTagName("form");

		$valid_forms = [];
 		foreach($forms as $form){
			if(preg_match('/(search|find|(?<!j)query|"q"|keyword|srch)/ui', 
					$this->dom->saveHTML($form))){
				$valid_forms[] = $form;
			}
		}
		return $valid_forms;
	}

	private function generateBoundary(){
		return "--". substr(md5(rand(0,32000)),0,10);
	}

	/**
	* Isprobaj formu i vrati Stranicu
	* @param Form $form 
	* @return Site
	*/
	private function submit(Form $form, $headers = array()){
		#Ovo mozda ne bi trebalo biti tu.. svaki put se stvara a u najvecem br slucajeva nije multipart
		$this->boundary = $this->generateBoundary();
		$body = $form->encodeData($this->boundary);
		
		$ret = null;
		switch($form->getMethod()){
			case 'GET':
				$url = $form->getAction();
				$url .= false !== strpos($url, '?')?'&':'?';
				$url .= $body;
				$this->site->setUrl($url);
				break;
			case 'POST':
				$this->site->setUrl($form->getAction());
				$this->site->setCurlPost($body);
				$this->postFields = $body;
				
				if('multipart/form-data' == $form->getEnctype()){
					$this->site->setCurlHeaders(
							array_merge(
									array("Content-Type: multipart/form-data; boundary=$this->boundary"), 
									$headers
									)
							);
				} else {
					$this->site->setCurlHeaders(
							array_merge(
									array("Content-Type: application/x-www-form-urlencoded"), 
									$headers
									)
							);
				}
				break;
			default: 
				throw new FormSiteSearchException("Nepoznata metoda: " . $form->getMethod());
		}
	}
    
	// </editor-fold>
}