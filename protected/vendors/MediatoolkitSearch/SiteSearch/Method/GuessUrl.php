<?php
namespace MediatoolkitSearch\SiteSearch\Method;

use MediatoolkitSearch\Common\Functions;
use MediatoolkitSearch\Common\Site;
use MediatoolkitSearch\SiteSearch\Interfaces\IRequest;
use MediatoolkitSearch\Common\URL;

/**
 * Description of OpenSearch
 *
 * @author Branimir
 */
class GuessUrl implements IRequest {
	/**
	 *
	 * @var Site Stranica nad kojom se pretražuje
	 */
	private $site;

	/**
	 *
	 * @var string URL template koji dobijemo iz XML-a
	 */
	private $URL;
	
	private $type = 40;

	public function __construct($site) {
		$this->site = $site;
		$this->setData();
	}

	public function getHeader(){
	}

	/**
	 * 
	 * @return URL
	 */
	public function getURL(){
		return $this->URL;
	}
	
	public function getFormType(){
		return 'GET';
	}
	
	public function getPostFields(){
		return '';
	}
	
	public function getBoundary(){
		return '';
	}
	
	public function getType(){
		return $this->type;
	}

	private function setData(){
		$keyword = 'OVONEPOSTOJIDEF';
		if($this->tryUrl('s', $keyword)){
			$this->URL = new URL(
					str_replace($keyword, '{searchTerms}', $this->site->getUrl())
			);
		}
	}
	
	private function tryUrl($tryName, $keyword){
		$this->site->setUrl(Functions::rel2abs("?$tryName=$keyword", $this->site->getUrl()));
		
		$ret = false;
		if(stripos($this->site->getHTML(), $keyword)){
			$ret = true;
		}
		return $ret;
	}
	
}