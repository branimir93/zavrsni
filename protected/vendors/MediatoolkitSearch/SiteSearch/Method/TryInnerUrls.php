<?php
namespace MediatoolkitSearch\SiteSearch\Method;

use MediatoolkitSearch\Common\Functions;
use MediatoolkitSearch\Common\Site;
use MediatoolkitSearch\SiteSearch\Interfaces\IRequest;
use MediatoolkitSearch\SiteSearch\Method\SearchForm;

/**
 * Description of OpenSearch
 *
 * @author Branimir
 */
class TryInnerUrls implements IRequest {
	
	private static $LINK_NUM = 3;

	/**
	 *
	 * @var Site Stranica nad kojom se pretražuje
	 */
	private $site;
	
	/**
	 *
	 * @var string Bazni url stranice
	 */
	private $url;
	
	/**
	 *
	 * @var SearchForm Iskoristiva search forma je pronađena na tom linku
	 */
	private $link = null;
	
	/**
	 *
	 * @var string[] Linkovi koje ćemo isprobati (biraju se u konstruktoru)
	 */
	private $testedLinks;
	
	private $type = 30;

	public function __construct($site, $links) {
		$this->site = $site;
		$this->url = $this->site->getUrl();
		$links = $this->chooseLinks($links);
		$this->setData($links);
	}

	public function getHeader(){
		if(!is_null($this->link)){ return $this->link->getHeader(); }
		else return false;
	}
	
	public function getURL(){
		if(!is_null($this->link)){ return $this->link->getURL(); }
		else return false;
	}
	
	public function getFormType(){
		if(!is_null($this->link)){ return $this->link->getFormType(); }
		else return false;
	}
	
	public function getPostFields(){
		if(!is_null($this->link)){ return $this->link->getPostFields(); }
		else return false;
	}
	
	public function getBoundary(){
		if(!is_null($this->link)){ return $this->link->getBoundary(); }
		else return false;
	}
	
	public function getType(){
		return $this->type;
	}
	
	private function chooseLinks($links){
		usort($links, function($a, $b) {
			return strlen($a) - strlen($b);
		});
		//filter links
		$parseBase = parse_url($this->url);
		$base = preg_quote(str_ireplace('www.', '', $parseBase['host']));
		$links2 = preg_grep("`$base.*(search|browse|index|home|srch|(\?|&)s=)`m", $links);
		$i = 0;
		foreach(array_reverse($links2) as $link){
			if(++$i>self::$LINK_NUM) { break; }
			array_unshift($links, $link);
		}
		
		$chosen = [];
		$i = 0;
		foreach($links as $url_try){
			$parsed = parse_url($url_try);

			$path = isset($parsed['path'])?$parsed['path']:'';
			$pathQuery = isset($parseBase['path'])?$parseBase['path']:'';

			$query = isset($parsed['query'])?
					preg_replace('/nojs=1&?/', '', $parsed['query']):
					'';
			$baseQuery = isset($parseBase['query'])?
					preg_replace('/nojs=1&?/', '', $parseBase['query']):
					'';

			if($path == $pathQuery && $query == $baseQuery) { continue; }

			if(++$i>self::$LINK_NUM) { break; }

			$chosen[] = $url_try;
		}
		
		return $chosen;
	}

	private function setData($links){
		foreach($links as $link){
			$this->testedLinks[] = $link;
			$this->site->setUrl($link);
			$ret = new SearchForm($this->site);
			if(!empty($ret->getURL())){
				$this->link = $ret;
				break;
			}
		}
	}
	
}