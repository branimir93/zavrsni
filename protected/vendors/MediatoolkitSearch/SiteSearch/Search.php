<?php
namespace MediatoolkitSearch\SiteSearch;

use MediatoolkitSearch\Common\Site;
use MediatoolkitSearch\Exception\MethodNotChosenException;
use MediatoolkitSearch\SiteSearch\Method\SearchForm;
use MediatoolkitSearch\SiteSearch\Method\OpenSearch;
use MediatoolkitSearch\SiteSearch\Method\GuessUrl;
use MediatoolkitSearch\SiteSearch\Method\TryInnerUrls;
use MediatoolkitSearch\SiteSearch\Interfaces\IRequest;
use MediatoolkitSearch\Exception\SiteSearchException;

/**
 * Description of Search
 *
 * @author Branimir
 */
class Search {
	
	/**
	 *
	 * @var Site
	 */
	private $site;
	
	/**
	 *
	 * @var IRequest
	 */
	private $method;
	
	private $url;

	public function __construct($url){
		$this->url = $url;
		$this->site = new Site($url);
		$this->method = $this->analyze();
	}

	public function analyze(){
		do {
			//da ubrzam zadnju pretragu
			$links = $this->site->getLinks();
			$errors = [];
			try {
				$ret = new OpenSearch($this->site);
				if(!empty($ret->getURL())) { break; }
			}
			catch(\Exception $ex){
				//$errors[] = $ex->getMessage();
			}
			
			try {
				$ret = new SearchForm($this->site);
				if(!empty($ret->getURL())) { break; }
			}
			catch(\Exception $ex){
				$errors[] = $ex->getMessage();
			}
			
			try {
				$this->site->setUrl($this->url);
				$ret = new TryInnerUrls($this->site, $links);
				if(!empty($ret->getURL())) { break; }
			} catch (\Exception $ex) {
				$errors[] = $ex->getMessage();
			}
			
			try {
				$this->site->setUrl($this->url);
				$ret = new GuessUrl($this->site);
				if(!empty($ret->getURL())) { break; }
			}
			catch(\Exception $ex){
				$errors[] = $ex->getMessage();
			}
			
			if(empty($errors)){
				throw new MethodNotChosenException(
						'Trenutno definiran skup pretraga ne može pronaći tražilicu. '
						. 'Moguće je da stranica koristi javascript ili da ne postoji '
						. 'tražilica na stranici.');
			}
			else {
				throw new MethodNotChosenException(implode("\t", $errors));
			}
			
		} while (0);
		
		return $ret;
	}
	
	public function getURL(){
		return $this->method->getURL();
	}

	public function getHeader(){
		return $this->method->getHeader();
	}
	
	public function getPostFields(){
		return $this->method->getPostFields();
	}
	
	public function getFormType(){
		return $this->method->getFormType();
	}
	
	public function getBoundary(){
		return $this->method->getBoundary();
	}
	
	public function getType(){
		return $this->method->getType();
	}
        
}
