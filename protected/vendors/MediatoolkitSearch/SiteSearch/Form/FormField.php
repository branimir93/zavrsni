<?php
namespace MediatoolkitSearch\SiteSearch\Form;

/**
 *
 * @author Branimir
 */
class FormField {
	
	public $name;
	
	public $value;
	
	private $type;
	
	/**
	 * 
	 * @param string $name
	 * @param string $value
	 * @param type $type
	 */
	public function __construct( $name,  $value, $type = 'hidden'){
		$this->name = $name;
		$this->value = $value;
		$this->type = $type;
	}
	
	public function getType(){
		return $this->type;
	}
}
