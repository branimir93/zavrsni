<?php
namespace MediatoolkitSearch\SiteSearch\Form;

/**
 *
 * @author Branimir
 */
class FormFieldList {
	
	/**
	 *
	 * @var FormField[]
	 */
	private $fields = array();
	
	public function getInputValue($name) {
		if(!is_null($this->fields[$name])){
			return $this->fields[$name]->value;
		}
		else {
			return '';
		}
	}

	public function add(FormField $field){
		$this->fields[$field->name] = $field;
	}
	
	public function count(){
		return count($this->fields);
	}
	
	public function changeValue( $name,  $value){
		$this->fields[$name] = new FormField($name, $value);
	}
	
	public function possibleQueryFields(){
		$ret = new FormFieldList();
		foreach ($this->fields as $field) {
			if(in_array($field->getType(), 
					['text', 'search']
					)
			){
				$ret->add($field);
			}
		}
		return $ret;
	}
	
	public function toArray() {
		$ret=array();
		foreach ($this->fields as $field){
			
			$ret[$field->name] = $field->value;
		}
		return $ret;
	}
}
