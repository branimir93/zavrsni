<?php
namespace MediatoolkitSearch\SiteSearch\Form;

use DOMElement;
use MediatoolkitSearch\SiteSearch\Form\FormFieldList;
use MediatoolkitSearch\Common\Functions;
use MediatoolkitSearch\SiteSearch\Exception\FormSiteSearchException;

/**
 *
 * @author Branimir
 */
class Form {
    
	/**
	 *
	 * @var string url stranice koju pretrazujem (za apsolutne adrese) 
	 */
	private $url;
	
	/**
	 * Polje svih fildova
	 * @var FormFieldList
	 */
	private $formData;
    
	/**
	 * Adresa na koju se forma salje
	 * @var string
	 */
    private $action;
    
	/**
	 * Metoda koju forma koristi (GET, POST)
	 * @var string
	 */
    private $method = 'GET';
	
	/**
	 * Enctype koji forma koristi za kodiranje
	 * @var string
	 */
	private $enctype = 'application/x-www-form-urlencoded';
	
	/**
	 * 
	 * @param DOMElement $domElement zadana forma koju pretrazujem
	 */
	public function loadByDOM(DOMElement $domElement, $url) {
		$this->url = $url;
		$this->getFormAttributes($domElement);
		
        $this->formData = $this->getFormFields($domElement);
    }
	
	/**
	 * 
	 * Postavlja atribute forme (actiom, method, enctype)
	 * 
	 * @param DOMElement $form zadana forma koju pretrazujem
	 */
	private function getFormAttributes(DOMElement $form){
		//Ako je Action prazan (iako ne smije biti) koristi kao da je zadano
		//action=""
		$this->setAction($form->getAttribute('action'));
		
		if ($form->hasAttribute('method')) {
			$this->setMethod($form->getAttribute('method'));
		}
		
		if ($form->hasAttribute('enctype')) {
			$this->setEnctype($form->getAttribute('enctype'));
		}
	}
    
	/**
	 * 
	 * Metoda vraca listu svih zadanih imputa i njihovih vrijednosti ako one 
	 * postoje
	 * 
	 * @param DOMElement $domElement Nad cemu se provodi pretraga
	 * @return FormField[] lista svih polja i vrijednosti (ako postoje) inace je
	 * vrijednost prazan string
	 */
    private function getFormFields(DOMElement $domElement){
		$formData = new FormFieldList();
		
		$inputs = $domElement->getElementsByTagName('input');
		foreach($inputs as $input){
			if ($input->hasAttribute('name')) {
				$formData->add($this->getInputData($input));
			}
		}
		
		$buttons = $domElement->getElementsByTagName('button');
		foreach($buttons as $button){
			if ($button->hasAttribute('name')) {
				$formData->add($this->getInputData($input));
			}
		}
		
		$selects = $domElement->getElementsByTagName('select');
		foreach($selects as $select){
			if ($select->hasAttribute('name')) {
				$formData->add($this->getSelectData($select));
			}
		}
		
		$textareas = $domElement->getElementsByTagName('textarea');
		foreach($textareas as $textarea){
			if ($textarea->hasAttribute('name')) {
				$formData->add($this->getTextareaData($textarea));
			}
		}
		
        return $formData;
    }
	
	private function getInputData(DOMElement $input){		
		$ret = new FormField(
			$input->getAttribute('name'), 
			$input->getAttribute('value'),
			$input->getAttribute('type')
		);

		if (empty($this->action) && $input->hasAttribute('formaction')){
			$this->setAction($input->getAttribute('action'));
		}

		if (empty($this->enctype) && $input->hasAttribute('enctype')){
			$this->setEnctype($form->getAttribute('enctype'));
		}

		if (empty($this->method) && $input->hasAttribute('method')){
			$this->setMethod($form->getAttribute('method'));
		}
		
		return $ret;
	}
	
	private function getTextareaData(DOMElement $textbox){
		return new FormField(
			$textbox->getAttribute('name'), 
			$textbox->nodeValue,
			$textbox->getAttribute('type')
		);
	}
	
	private function getSelectData(DOMElement $select){
		$options = $select->getElementsByTagName('option');
		$firstVal = $options->length?$options->item(0)->getAttribute('value'):'';
		if($select->hasAttribute('multiple')){
			$selectedOptions[] = $firstVal;
			$end = false;
		}
		else{
			$selectedOptions = $firstVal;
			$end = true;
		}

		foreach ($options as $option) {
			if($option->hasAttribute('selected')){
				if($end){
					$selectedOptions = $option->getAttribute('value');
					break;
				}
				else {
					$selectedOptions[] = $option->getAttribute('value');
				}
			}
		}

		return new FormField(
			$select->getAttribute('name'), 
			$selectedOptions,
			$select->getAttribute('type')
		);
	}
	
	public function encodeData($boundary){
		$ret = '';
		if($this->enctype === 'multipart/form-data' && 
				$this->method === 'POST'){
			$ret = $this->multipartBuildQuery($boundary);
		}
		else {
			$ret = http_build_query($this->formData->toArray(), 
					'', '&', PHP_QUERY_RFC1738);
		}
		
		return $ret;
	}
	
	private function multipartBuildQuery($boundary){
		$ret = '';
		foreach($this->formData->toArray() as $key => $value){
			$ret .= "--$boundary\nContent-Disposition: form-data; name=\"$key\"\n\n$value\n";
		}
		$ret .= "--$boundary--";
		return $ret;
	}
	
	public function getAction(){
		return $this->action;
	}
	
	public function getMethod(){
		return $this->method;
	}
	
	public function getEnctype(){
		return $this->enctype;
	}
	
	public function getFormData(){
		return $this->formData;
	}
	
	public function setAction($action){
		$this->action = Functions::rel2abs(
				$action,
				$this->url
		);
	}
	
	public function setMethod($method) {
		$this->method = strtoupper($method);
	}
	
	public function setEnctype($enctype) {
		$this->enctype = $enctype;
	}
	
}
