<?php
namespace MediatoolkitSearch\SiteSearch\Interfaces;

interface IRequest {

    /**
     * @return \Site\Common\URL
     */
    public function getURL();
    
    /**
     * @return \Site\Common\Header
     */
    public function getHeader();
	
	/**
	 * @return string vrsta zahtjeva (POST ili GET)
	 */
	public function getFormType();
	
	/**
	 * @return string Ukoliko je vrsta zahtjeva POST, vrati njegove post_fieldove
	 */
	public function getPostFields();
	
	/**
	 * @return string boundary koji se koristi u multipart zahtjevima
	 */
	public function getBoundary();
	
	/**
	 * @return int Šifra vrste dohvaćene tražilice
	 */
	public function getType();

}