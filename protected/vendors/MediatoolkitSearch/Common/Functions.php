<?php

namespace MediatoolkitSearch\Common;

/**
 * Funkcije korištene unutar SiteSearcha
 *
 * @author Branimir
 */
class Functions {
	/**
	 * Omogučuje pretvorbu relativne adrese u absolutnu, također provjerava
	 * je li kao relativna adresa predana absolutna te ukoliko je vraća ju.
	 * 
	 * Primjer korištenja:
	 * <pre><code>
	 * echo rel2abs("http://www.example.com/dir/page.html","http://www.example.com/");
	 * // Output: http://www.example.com/dir/page.html
	 * 
	 * echo rel2abs("/dir/page.html","http://www.example.com/");
	 * // Output: http://www.example.com/dir/page.html
	 *
	 * echo rel2abs("/dir/page.html","http://www.example.com/dir1/page2.html");
	 * // Output: http://www.example.com/dir/page.html
	 *
	 * echo rel2abs("dir/page.html","http://www.example.com/dir1/page2.html");
	 * // Output: http://www.example.com/dir1/dir/page.html
	 *
	 * echo rel2abs("../dir/page.html","http://www.example.com/dir1/dir3/page.html");
	 * // Output: http://www.example.com/dir1/dir/page.html
	 * </code></pre>
	 * 
	 * @param string $rel relativna adresa
	 * @param string $base bazna adresa stranice u formatu http://www.example.com
	 * @return string
	 */
	public static function rel2abs($rel, $base)
	{
		if(empty($rel)){
			return $base;
		}
		if (filter_var($rel, FILTER_VALIDATE_URL)){
			return $rel;
		}

		if(strpos($rel,"//")===0){
			return "http:".$rel;
		}

		/* return if  already absolute URL */
		if  (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;

		/* queries and  anchors */
		if ($rel[0]=='#') return $base.$rel;
		if($rel[0]=='?') {
			$pos = strpos($base, '?');
			if(false!==$pos){
				if($pos!=strlen($base)-1)
					$append = '&';
			}
			else {
				$append = '?';
			}
			$append .= substr($rel, 1);
			return $base.$append;
		}

		$path = '';
		$host = '';
		$scheme = '';

		/* parse base URL  and convert to local variables:
		$scheme, $host,  $path */
		extract(parse_url($base));

		/* remove  non-directory element from path */
		$path = preg_replace('#/[^/]*$#',  '', $path);

		/* destroy path if  relative url points to root */
		if ($rel[0] ==  '/') $path = '';

		/* dirty absolute  URL */
		$abs =  "$host$path/$rel";

		/* replace '//' or  '/./' or '/foo/../' with '/' */
		$re =  array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
		for($n=1; $n>0;  $abs=preg_replace($re, '/', $abs, -1, $n)) {}

		/* replace www.example.com/..(/) with www.example.com */
		for($n=1; $n>0;  $abs=preg_replace('#' . $host . '/\.\.(/?)#', 
												 $host . '\1', $abs, -1, $n)) {}
		
		/* absolute URL is  ready! */
		return  $scheme.'://'.$abs;
	}
	
	public static function getDomainName($url){
		$ret = array();
		$host = '';
		extract(parse_url($url));
		preg_match('#([^\.]+)\.[^\.]*$#m', $host, $ret);
		
		return $ret[1];
	}
}