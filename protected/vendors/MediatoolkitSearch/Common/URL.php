<?php
namespace MediatoolkitSearch\Common;

/**
 * Description of URL
 *
 * @author Branimir
 */
class URL {
        
	/**
	 *
	 * @var string
	 */
	private $url;
	
	/**
	 * cache za funkciju getSearchTermName()
	 * @var string
	 */
	private $searchTermName=null;

	/**
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->url;
	}

	/**
	 * 
	 * @param string $url
	 */
	public function __construct($url){
		$this->url = $url;
	}
	
	public function getSearchTermName(){
		if(is_null($this->searchTermName)){
			$tmp = preg_replace('`.*[&\?]([^&\?\*=]+)=(?=\{searchTerms\}).*`i',
					'$1', $this->url);
			return $tmp==$this->url?'':$tmp;
		}
		else {
			return $this->searchTermName;
		}
	}
	
	/**
	 * prilika za nadogradnju (ukoliko postoji već unutar urla tag za paginaciju
	 * iskoristi to)
	 * @return boolean
	 */
	public function havePagination(){
		return false;
	}
        
}