<?php
namespace MediatoolkitSearch\Common;

use MediatoolkitSearch\Exception\SiteNotAvailableException;

/**
 * Description of Site
 *
 * @author Branimir
 */
class Site {
        
	/*
	 * @var string URL stranice dobiven prilikom stvaranja objekta, no 
	 * ukoliko dolazi do redirekcije i taj parametar se mijenja (također, može
	 * se mijenjati pomoću setUrl() a zatim se $html ponistava i mijenja se curl
	 */
	private $url;
	
	/**
	 *
	 * @var string Polja poslana u POST zahtjevu
	 */
	private $postFields = '';

	/*
	 * @var string HTML kod stranice
	 */
	private $html;

	/*
	 * @var resource
	 */
	private $ch;
	
	/**
	 * 
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getPostFields(){
		return $this->postFields;
	}
	
	/**
	 * Dohvaća zadnji poslani header poslan u zahtjevu za stranicom
	 * @return string
	 */
	public function getLastHeader(){
		return curl_getinfo($this->ch, CURLINFO_HEADER_OUT);
	}
	
	/**
	 * Postavlja novi URL, ujedno stavlja html na null (kako bi se natjeralo da
	 * pri sljedecem pozivu getHTML() pozovemo fetchHTML()
	 * Dodaje nojs da ukoliko stranica posjeduje nojs iskoristimo to
	 * @param string $url
	 */
	public function setUrl($url) {
		if(strpos($url, 'nojs=1') === false){
			$this->url = strpos($url, '?')===false?$url.'?nojs=1':$url.'&nojs=1';
		}
		else{
			$this->url = $url;
		}
		
		$this->html = null;
		$this->initCurl();
	}
	
	/**
	 * Dohvaća vrijednost HTML parametra, ukoliko nije postavljena poziva 
	 * fetchHTML()
	 * @return string
	 */
	public function getHTML(){
		if(is_null($this->html)){
			$this->fetchHTML();
		}
		
		return $this->html;
	}
	
	/**
	 * @param string $url URL stranice koju se želi pretražiti
	 */
	public function __construct($url) {
		$this->setUrl($url);
		$this->initCurl();
	}
	
	/**
	 * Uništava curl objekt i zatvara vezu
	 */
	public function __destruct() {
		$this->closeCurl();
	}
	
	public function __toString() {
		return $this->html;
	}
	
	/**
	 * Inicijalizira Curl ukoliko on vec nije postavljen te resetira sve 
	 * postavke na pocetno stanje
	 */
	private function initCurl(){
		if(is_null($this->ch)){
			$this->ch = curl_init($this->url);
		} 
		
		$this->resetCurlOptions();
	}
	
	private function closeCurl(){
		curl_close($this->ch);
	}
	
	/**
	 * Dohvaća HTML stranice ukoliko je ona dostupna, ako nije baca grešku
	 * Poziva se ako ne želimo koristiti već dohvaćeni HTML (za to pogledaj 
	 * getHTML())
	 * Ukoliko je doslo do redirekcijemijenja url.
	 * @throws SiteNotAvailableException
	 */
	public function fetchHTML(){
		
		$response = curl_exec($this->ch);
		
		//izdvoji header
		$header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		
		//ispitaj da li se pojavio error
		$err = curl_errno($this->ch);
		if(empty($err)){
			// Uzmi zadnju adresu (zbog mogucih redirektova)
			if(preg_match_all('#Location: (.*)#i', $header, $r)){
				$this->setUrl(
					Functions::rel2abs(trim($r[1][count($r[1])-1]), $this->url)
				);
			}
		}
		else {
			throw new SiteNotAvailableException(
					curl_strerror($err)
					#'Stranica ne postoji ili trenutno nije dostupna'
			);
		}
		
		//Nakon provjere redirekta zato što postavljaanje urla briše HTML
		$this->html = substr($response, $header_size);
		
	}
	
	public function resetCurlOptions(){
		curl_setopt($this->ch, CURLOPT_URL, $this->url); 
		
		//redirekcije
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
		
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true); //vrati kao string
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 20); // 20 sekundi
		curl_setopt($this->ch, CURLINFO_HEADER_OUT, true); //dohvacanje poslanih header
		curl_setopt($this->ch, CURLOPT_VERBOSE, true);
		curl_setopt($this->ch, CURLOPT_HEADER, true); // ispiši vraćeni header
		
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
		#curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Mediatoolkit search/0.0.1;)');
		curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip,deflate,identity');
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			"Accept-Charset:	ISO-8859-1,utf-8;q=0.7,*;q=0.7",
			"Accept-Language:	en-us,en;q=0.5",
			"Connection: keep-alive",
			"Keep-Alive: 300",
		));
		
		curl_setopt($this->ch, CURLOPT_POST, false);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, '');
		curl_setopt($this->ch, CURLOPT_HTTPGET, true);
		$this->postFields = '';
	}
	
	/**
	* Set curl headers
	*/
	public function setCurlHeaders($headers){
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
	}
	
	/**
	 * Pripremanje curl-a za slanje post forme
	 * @param string $body niz parametara prpipremljen za slanje u postFields
	 * zavisno o kodiranju forme. npr a=b&c=d
	 */
	public function setCurlPost($body){
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $body);
		$this->postFields = $body;
	}
	
	/**
	 *
	 * @var string Regex korišten za pronalazak linkova
	 */
	public static $regex = '#<a(\s+|\s+[^>]*\s+)href\s*=\s*(["\'])([^\2]+)\2[^>]*>#siUu';
	
	/**
	 * Pronalazi sve linkove na stranici, prepravlja relativne putanje u 
	 * apsolutne.
	 * Ukoliko link nije na istu stranicu ne uzima ga u obzir
	 * 
	 * @return string[]
	 */
	public function getLinks($count_items=false) {
		$matches = array();

		$tmp = parse_url($this->url);
		$url = str_ireplace('www.', '', $tmp['host']);
		
		if(preg_match_all(self::$regex, $this->getHTML(), $matches, PREG_PATTERN_ORDER)){
			foreach($matches[3] as $k=>$match){
				$matches[3][$k] = Functions::rel2abs($match, $this->url);

				/* Ako je isti link nakon pretvorbe => $match je apsolutna
				 * putanja.. provjeri jel treba izbaciti(je li sa vanjske
				 * stranice)
				 * Takodjer, makni sve linkove koji sadrzavaju share= (obicno su to duplikatu)
				 * Takodjer, makni mailto
				 * takodjer ako piocinje sa #
				 */
				$urlParsed = parse_url($matches[3][$k]);
				if(empty($urlParsed['host'])) {$urlParsed['host'] = '';}
				if($match === $matches[3][$k] && 
						false === strpos($urlParsed['host'], $url) ||
						false !== strpos($matches[3][$k], '@') ||
						false !== strpos($matches[3][$k], 'share=') || #pokušaj uklanjanja share linkova
						false !== strpos($matches[3][$k], '/tag/') ||
						$matches[3][$k][0] == '' ||
						$matches[3][$k][0] == '#'){
					unset($matches[3][$k]);
				}						
				else {
					//Makni #dio iz linka
					$tmp = strstr($matches[3][$k], '#', true);
					$matches[3][$k] =  false !== $tmp?$tmp:$matches[3][$k];
				}
			}
		}
		
		if($count_items){
			return array_count_values($matches[3]);
		}
		else {
			return array_unique($matches[3]);
		}
	}
        
}