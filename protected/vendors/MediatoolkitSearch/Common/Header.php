<?php

namespace MediatoolkitSearch\Common;

/**
 * Description of Header
 *
 * @author Branimir
 */
class Header {
        
	/**
	 *
	 * @var string[]
	 */
	private $header_fields = array();

	public function __toString(){
		return implode('&', $this->header_fields);
	}
}