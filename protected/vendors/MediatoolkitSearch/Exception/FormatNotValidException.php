<?php
namespace MediatoolkitSearch\Exception;

use MediatoolkitSearch\Exception\Exception;

/**
 * Description of FormatNotValidException
 *
 * @author Branimir
 */
class FormatNotValidException extends SiteSearchException {
        
}