<?php
namespace MediatoolkitSearch\Exception;

/**
 * Description of SiteNotAvailableException
 *
 * @author Branimir
 */
class SiteNotAvailableException extends SiteSearchException {
        
}