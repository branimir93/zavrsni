<?php
namespace MediatoolkitSearch\Exception;

use MediatoolkitSearch\Exception\FormatNotValidException;

/**
 * Description of HTMLFormatNotValidException
 *
 * @author Branimir
 */
class HTMLFormatNotValidException extends FormatNotValidException {
        
}