<?php
namespace MediatoolkitSearch\Exception;

use MediatoolkitSearch\Exception\FormatNotValidException;

/**
 * Description of XMLFormatNotValidException
 *
 * @author Branimir
 */
class XMLFormatNotValidException extends FormatNotValidException {
        
}