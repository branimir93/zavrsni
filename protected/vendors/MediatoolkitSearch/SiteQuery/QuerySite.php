<?php
namespace MediatoolkitSearch\SiteQuery;

use MediatoolkitSearch\Common\Site;

/**
 *
 * @author Branimir
 */
class QuerySite extends Site{
	
	public function __construct($params) {
		parent::__construct($params['url']);
		if(!empty($params['postFields'])){
			$this->setCurlPost($params['postFields']);
		}
		if(!empty($params['boundary'])){
			$this->setCurlHeaders(array('Content-Type: multipart/form-data; boundary=' . $params['boundary']));
		}
		else {
			$this->setCurlHeaders(array("Content-Type: application/x-www-form-urlencoded"));
		}
	}
                
}