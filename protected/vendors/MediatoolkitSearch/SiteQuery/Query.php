<?php
namespace MediatoolkitSearch\SiteQuery;

use MediatoolkitSearch\Common\URL;
use MediatoolkitSearch\Common\Header;
use MediatoolkitSearch\Common\Functions;

/**
 * Description of Query
 *
 * @author Branimir
 */
class Query {
    
	private static $MAX_PAGES = 2;
	
	/**
	 *
	 * @var string
	 */
	public $baseUrl;

	/**
	 *
	 * @var URL
	 */
	private $url;

	/**
	 *
	 * @var string
	 */
	private $keyword;
	
	private $formType;
	
	private $postFields;
	
	private $boundary;

	/**
	 * 
	 * @param URL $url
	 * @param Header $header
	 * @param string $baseUrl
	 * @param string $keyword
	 */
	public function __construct($url, $baseUrl, $keyword, $formType = 'GET', $postFields = '', $boundary = ''){
		$this->url = $url;
		$this->keyword = $keyword;
		$this->baseUrl = $baseUrl;
		$this->formType = $formType;
		$this->postFields = $postFields;
		$this->boundary = $boundary;
	}
	
	/**
	 * 
	 * Šalje dva zahtjeva za stranicom, jedan sa potrebnom kljucnom rijeci, a 
	 * jedan sa nekom izmisljenom za koju nece postojati rezultati te radi 
	 * razliku dva skupa linkova te tako ostaju korisni rezultati i linkovi
	 * vezani uz pretragu (paginacija, filtriranje)
	 * 
	 * @return array
	 */
	public function getResults(){
		$fakeKeyword = 'zaOvoSigurnoNemaRezultata';
		$b = new QuerySite($this->getFinalParams($fakeKeyword));
		$noResults = $b->getLinks(true);
		unset($b);
		
		$params = $this->getFinalParams($this->keyword);
		$results = $this->getPaginatedResults($noResults, $fakeKeyword, $params, 1);
		
		
		return $results;
	}
	
	private function getPaginatedResults(&$noResults, &$fakeKeyword, $params, $page){
		if($page <= self::$MAX_PAGES){
			$page++;
			$a = new QuerySite($params);
			$myResults = $a->getLinks(true);

			/**
			 * ovo me stiti u slucaju da postoji sa strane nekakav popis
			 * najnovijih stranica, a rezultat je unutar njih
			 */
			$resultsWithSearchLinks=array_keys(array_diff_assoc($myResults, $noResults));
			
			$dynamicContent = array_keys(array_diff_assoc($noResults, $myResults));
			unset($myResults, $a);
			$enablePagination = true;
			if(!empty($dynamicContent)){
				//Provjeri treba li omogučiti paginaciju
				$fakeOtherSearchLinks = $this->getSearchLinks($dynamicContent, $fakeKeyword);
				$fakeUrl = $this->chooseNextPageUrl($fakeOtherSearchLinks['useful'], $page);
				if($fakeUrl) { $enablePagination = false; }
				
				//Nadodaj provjeru za dinamični sadržaj
			}
			
			$otherSearchLinks = $this->getSearchLinks($resultsWithSearchLinks, $this->keyword);
			$results = array_diff($resultsWithSearchLinks, $otherSearchLinks['useful'], $otherSearchLinks['notMine']);
			
			$url = $this->chooseNextPageUrl($otherSearchLinks['useful'], $page);
			if($enablePagination && $url){
				$params = $this->getFinalParamsPagination($url);
				$paginatedResults = $this->getPaginatedResults($noResults, $fakeKeyword, $params, $page);
				return array_unique(array_merge($results, $paginatedResults));
			}
			else {
				return $results;
			}
		}
		return [];
	}
	
	private function chooseNextPageUrl($otherSearchLinks, $page) {
		$tmp = preg_grep("`=$page(&|$)?|/$page/`im", $otherSearchLinks);
		$vrati = '';
		foreach($tmp as $link){
			if(preg_match("`(\?|&?)(page|p|pageNumber|pn)=$page(&|$)?|/$page/|`im", $link)){
				return $link;
			}
			$vrati = $link;
		}
		if(empty($vrati)) { return false; }
		else { return $vrati; }
	}
	
	private function getSearchLinks($resultsWithSearchLinks, $keyword){
		$domainName = preg_quote(Functions::getDomainName($this->baseUrl));
		$keyword = preg_quote(urlencode($keyword));
		
		if(preg_match('#/\{searchTerms\}#i', (string)$this->url)){
			$url = preg_quote(preg_replace('`(https?://[^\{]+).*`i', '$1', (string)$this->url));
			return [
				'useful' => preg_grep(
					"`($url|$domainName.*search).*$keyword`i", 
					$resultsWithSearchLinks),
				'notMine' => preg_grep(
					"`$url(?!$keyword)`i", 
					$resultsWithSearchLinks),
			];
		}
		else {
			$name = preg_quote($this->url->getSearchTermName());
			$url = preg_quote(preg_replace('`(https?://[^\?]+).*`i', '$1', (string)$this->url));
			return [
				'useful' => preg_grep(
					"`($url|$domainName.*search).*(\?|&)$name=$keyword(&|$)?`i", 
					$resultsWithSearchLinks),
				'notMine' => preg_grep(
					"`($url|$domainName.*search).*(\?|&)$name=(?!$keyword)`i", 
					$resultsWithSearchLinks),
			];
		}
	}
	
	public function getFinalParamsPagination($url){
		return [
			'url' => $url,
			'postFields' => '',
			'boundary' => ''
		];
	}
	
	public function getFinalParams($keyword){
		return [
			'url' => $this->getFinalUrl($keyword),
			'postFields' => $this->getFinalPostFields($keyword),
			'boundary' => $this->boundary
		];
	}


	/**
	 * 
	 * Vraca url napravljen prema templateu
	 * 
	 * @param string $keyword
	 * @return string
	 */
	private function getFinalUrl($keyword){
		return str_ireplace('{searchTerms}', urlencode($keyword), (string)$this->url);
	}
	
	private function getFinalPostFields($keyword){
		return str_ireplace('{searchTerms}', urlencode($keyword), (string)$this->postFields);
	}
        
}
