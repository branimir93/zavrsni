<?php

class SiteController extends Controller
{
        
        public $layout='//layouts/column2';
        
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$model = new Query;
		$smodel = new Site;

		$this->render('index',array(
			'model'=>$model,
			'smodel'=>$smodel,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}