<?php

Yii::setPathOfAlias('MediatoolkitSearch', Yii::getPathOfAlias('application.vendors.MediatoolkitSearch'));

class QueryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('update','index','create','delete','view','getLinks'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$result=new QueryResult('search');
		$result->unsetAttributes();  // clear any default values
		if(isset($_GET['QueryResult']))
			$result->attributes=$_GET['QueryResult'];
		
		$grid_id = 'query-result-grid';

		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial('_gridview_results', array(
				'model_id'=>$id,
				'result'=>$result,
			));
			Yii::app()->end();
		}
		
		$model = $this->loadModel($id);
		$siteDataProvider = new CArrayDataProvider($model->querySites);
		$siteDataProvider->setPagination(false);
                
		$this->render('view',array(
			'model'=>$model,
			'result'=>$result,
			'siteDataProvider'=>$siteDataProvider,
			'model_id'=>$id,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Query;
        $smodel = new Site;
		if(isset($_POST['Query']))
		{       
			$transaction = Yii::app()->db->beginTransaction();
			try{
				if(!isset($_POST['Query']['sites']) || !count($_POST['Query']['sites'])){
					$model->addError ('sites', 'Mora biti odabrana barem jedna stranica');
				}

				$keyword = new Keyword();
				if(isset($_POST['Query']['keyword']))
					$keyword = Keyword::getOldOrNewKeyword($_POST['Query']['keyword']);

				if(!$keyword->save()){
					$model->addError('keyword', $keyword->getError('Dogodila se pogreška prilikom pokušaja pretrage!'));
				}

				$model->keyword_id = $keyword->id;
				if($model->hasErrors() || !$model->save(true)){
					$model->keyword = $keyword->text;
					throw new Exception;
				}

				foreach($_POST['Query']['sites'] as $site){
					$qs = new QuerySite();
					$qs->site_id = $site;
					$qs->query_id = $model->id;
					$qs->status_id = 1; #  POCETNI
					$qs->save();
				}

				$transaction->commit();
				$this->redirect(array('view','id'=>$model->id));
			}
			catch (Exception $e){
				$transaction->rollback();
			}
		}
		$this->render('application.views.site.index', array(
			'model'=>$model,
			'smodel'=>$smodel,
		));
	}
        
	public function actionGetLinks($id){
		session_write_close();
		$qs = $this->loadQuerySite($id);
		if($qs->status_id == 1){
			$this->renderJSON($qs->getAndSaveLinks());
		}
		else {
			$this->renderJSON($qs->getResponse());
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Query('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Query']))
			$model->attributes=$_GET['Query'];
                
		$this->render('index',array(
			'model'=>$model,
		));
	}
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return QuerySite the loaded model
	 * @throws CHttpException
	 */
	public function loadQuerySite($id)
	{
		$model=QuerySite::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Query the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Query::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Query $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='query-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
