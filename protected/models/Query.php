<?php

/**
 * This is the model class for table "query".
 *
 * The followings are the available columns in table 'query':
 * @property integer $id
 * @property string $time
 * @property integer $keyword_id
 * @property string $post_fields
 *
 * The followings are the available model relations:
 * @property Keyword $keyword
 * @property QuerySite[] $querySites
 * @property Site[] $sites
 * @property QueryResult[] $results
 */
class Query extends CActiveRecord
{
	/**
	 *
	 * @var string
	 */
	public $keyword_text;

	/**
	 *
	 * @var string
	 */
	public $sites_titles;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('keyword_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('time, keyword_text, sites_titles', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'keyword' => array(self::BELONGS_TO, 'Keyword', 'keyword_id'),
			'querySites' => array(self::HAS_MANY, 'QuerySite', 'query_id'),
			'results' => array(
				self::HAS_MANY, 'QueryResult', 
				array('id'=>'query_site_id'), 
				'through'=>'querySites'
			),
			'sites' => array(self::MANY_MANY, 'Site', 'query_site(query_id, site_id)'),
		);
	}
        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'time' => 'Vrijeme',
			'keyword_id' => 'Pojam id',
			'keyword' => 'Pojam',
			'sites' => 'Stranice'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array(
			'keyword',
			'sites' => array(
				'select'=>false,
				'together'=>true
			),
		);
		$criteria->compare('time', $this->time, true);
		$criteria->compare('keyword.text', $this->keyword_text, true);
		$criteria->compare('sites.title', $this->sites_titles, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	/**
	 * 
	 * Dohvati zadnje upite i formatiraj ih za ispis u meniju
	 * 
	 * @param int $num koliko zadnjih Upita želimo
	 * @return array
	 */
	public static function getRecentQueries($num = 5){
			$recentQueriesModel = Query::model()->findAll(
				array(
					'order'=>'time DESC',
					'limit'=>$num
				)
			);

			$recentQueries = array();
			foreach($recentQueriesModel as $rqm){
				array_push($recentQueries, 
					array(
						'label'=>$rqm->keyword->text, 
						'url'=>array('query/view', 'id'=>$rqm->id)
					)
				);
			}

			return $recentQueries;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Query the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
