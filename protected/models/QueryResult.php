<?php

/**
 * This is the model class for table "query_result".
 *
 * The followings are the available columns in table 'query_result':
 * @property integer $id
 * @property integer $query_site_id
 * @property string $url
 *
 * The followings are the available model relations:
 * @property QuerySite $querySite
 */
class QueryResult extends CActiveRecord
{
	/**
	 *
	 * @var string
	 */
	public $site_name;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query_result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('query_site_id', 'numerical', 'integerOnly'=>true),
			array('url', 'url' ), 
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('site_name, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'querySite' => array(self::BELONGS_TO, 'QuerySite', 'query_site_id', 'joinType'=>'INNER JOIN'),
			'query' => array(self::BELONGS_TO, 'Query', array('query_id'=>'id'), 'through'=>'querySite', 'joinType'=>'INNER JOIN'),
			'site' => array(self::BELONGS_TO, 'Site', array('site_id'=>'id'), 'through'=>'querySite', 'joinType'=>'INNER JOIN'),
			'keyword' => array(self::BELONGS_TO, 'Keyword', array('keyword_id'=>'id'), 'through'=>'query', 'joinType'=>'INNER JOIN'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'query_site_id' => 'Query Site',
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 * 
         * @param int $query_id
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($query_id)
	{
		$criteria=new CDbCriteria;
                
		$criteria->condition = 'querySite.query_id=:query_id';
		$criteria->params = array(':query_id' => $query_id);
		$criteria->with = array(
			'site',
		);
		$criteria->compare('site.title',$this->site_name,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueryResult the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
