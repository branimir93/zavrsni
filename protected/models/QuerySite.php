<?php

/**
 * This is the model class for table "query_site".
 *
 * The followings are the available columns in table 'query_site':
 * @property integer $id
 * @property integer $query_id
 * @property integer $site_id
 * @property integer $status_id
 * @property string $query_start
 * @property string $query_finish
 * @property integer $query_result_num
 *
 * The followings are the available model relations:
 * @property QueryResult[] $queryResults
 * @property Query $query
 * @property Site $site
 * @property QuerySiteStatus $status
 */
class QuerySite extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query_site';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('query_id, site_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, query_id, site_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'queryResults' => array(self::HAS_MANY, 'QueryResult', 'query_site_id'),
			'query' => array(self::BELONGS_TO, 'Query', 'query_id'),
			'keyword' => array(self::BELONGS_TO, 'Keyword', array('keyword_id'=>'id'), 'through'=>'query', 'joinType'=>'INNER JOIN'),
			'site' => array(self::BELONGS_TO, 'Site', 'site_id'),
			'status' => array(self::BELONGS_TO, 'QuerySiteStatus', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'query_id' => 'Query',
			'site_id' => 'Site',
		);
	}
        
	public function saveResults($results){
		foreach($results as $result){
			//Osiguraj se od krivih urlova
			try {
				$qr = new QueryResult();
				$qr->url = $result;
				$qr->query_site_id = $this->id;
				$qr->save();
			} catch (Exception $e){
				
			}
		}
	}
	
	public function getResponse(){
		return [
			'result_num' => $this->query_result_num,
			'query_start' => $this->query_start,
			'query_finish' => $this->query_finish,
			'status' => $this->status->text,
		];
	}
	
	public function getAndSaveLinks(){
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$this->status_id = 2;
			$this->query_start = date('Y-m-d H:i:s');
			if($this->save()){
				$site = Site::model()->findByPk($this->site_id);
				if($site->form_type == 'POST'){
					$query = new MediatoolkitSearch\SiteQuery\Query(
						new MediatoolkitSearch\Common\URL($site->url_template),
						$site->url,
						$this->query->keyword->text,
						$site->form_type,
						$site->getSitePost()->post_fields,
						$site->getSitePost()->boundary
					);
				}
				else {
					$query = new MediatoolkitSearch\SiteQuery\Query(
						new MediatoolkitSearch\Common\URL($site->url_template),
						$site->url,
						$this->query->keyword->text
					);
				}
				
				$results = $query->getResults();
				$this->saveResults($results);

				$this->status_id = 3;
				$this->query_result_num = count($results);
				$this->query_finish = date('Y-m-d H:i:s');
				if($this->save()){
					$transaction->commit();
					return $this->getResponse();
				}
				else {
					throw new Exception;
				}
			}
			else {
				throw new Exception('Nije moguće pohraniti podatke u bazu');
			}
		}
		catch (Exception $e){
			$transaction->rollback();
			$this->query_finish = date('Y-m-d H:i:s');
			$this->status_id = 4;
			$this->save();
			return $this->getResponse();
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('query_id',$this->query_id);
		$criteria->compare('site_id',$this->site_id);
		
		$criteria->condition = 'querySite.query_id=:query_id';
		$criteria->params = array(':query_id' => $query_id);
		$criteria->with = array(
			'query',
			'queryResults',
			'keyword',
			'status',
		);
		$criteria->compare('site.title',$this->site_name,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuerySite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
