<?php

/**
 * This is the model class for table "site".
 *
 * The followings are the available columns in table 'site':
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $date_added
 * @property integer $type
 * @property string $url_template
 * @property string $form_type POST|GET
 *
 * The followings are the available model relations:
 * @property QuerySite[] $querySites
 * @property Query[] $queries 
 */
class Site extends CActiveRecord
{
	
	private $sitePost = null;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type', 'numerical', 'integerOnly'=>true, 'max'=>1000, 'min'=>0),
			array('title', 'length', 'max'=>45),
			array('date_added, url_template', 'safe'),
			array('url', 'url' ),
			array('url', 'length', 'max'=>255),
			array('url, title', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('url, title, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'querySites' => array(self::HAS_MANY, 'QuerySite', 'site_id'),
			'queries' => array(self::MANY_MANY, 'Query', 'query_site(site_id, query_id )'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'URL',
			'title' => 'Naslov',
			'date_added' => 'Datum Dodavanja',
			'type' => 'Type',
			'url_template' => 'Url Template',
		);
	}
	
	public function getSitePost(){
		if(is_null($this->sitePost)){
			$this->sitePost = SitePost::model()->find(
					'site_id=:site_id', ['site_id'=>$this->id]
			);
		}
		return $this->sitePost;
	}
	
	public function createSite() {
		$transaction = Yii::app()->db->beginTransaction();
		try{
			$search = new MediatoolkitSearch\SiteSearch\Search($this->url);
			$this->url_template = $search->getURL();
			$this->form_type = $search->getFormType();
			$this->type = $search->getType();
			if($this->save()){
				if($this->form_type == 'POST'){
					$site_post = new SitePost();
					$site_post->site_id = $this->id;
					$site_post->post_fields = $search->getPostFields();
					$site_post->boundary = $search->getBoundary();
					if(!$site_post->save()){
						throw new Exception('Pogreška pri spremanju POST parametara');
					}
				}
				$transaction->commit();
				return true;
			}
		}
		catch(Exception $e){
			$transaction->rollback();
			$this->addError('url', $e->getMessage());
			return false;
		}
	}
	
	public function fixUrl(){
		$parsed = parse_url($this->url);
		if(empty($parsed['scheme'])){
			$parsed['scheme'] = 'http';
		}
		if(empty($parsed['host']) && !empty($parsed['path'])){
			$parsed['host'] = $parsed['path'];
			$parsed['path'] = '';
		}
		$port = '';
		if(!empty($parsed['port'])){
			$port = ':' + $parsed['port'];
		}
		$query = '';
		if(!empty($parsed['query'])){
			$query = '?' + $parsed['query'];
		}
		$path = '/';
		if(!empty($parsed['path']) && $parsed['path'][0] != '/'){
			$path = '/' + $parsed['path'];
		}
		elseif(!empty($parsed['path']) && $parsed['path'][0] == '/'){
			$path = $parsed['path'];
		}
		$this->url = $parsed['scheme'] . '://' . $parsed['host'] . $port . 
				$path .  $query;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date_added',$this->date_added,true);

		$data = new CActiveDataProvider($this, array(
			'criteria'=>$criteria
		));
		$data->setPagination(false);
		return $data;
	}
    

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Site the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
