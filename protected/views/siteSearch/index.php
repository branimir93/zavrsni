<?php
/* @var $this SiteSearchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Stranice',
);

$this->menu=array(
	array('label'=>'Dodaj novu stranicu', 'url'=>array('create')),
);
?>

<h1>Sve stranice</h1>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'site-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => 'Prikazujem {start}-{end} od {count}',
        'emptyText' => 'Ne postoji ni jedna stranica.',
	'columns'=>array(
		array(
			'header'=>'#',
			'class'=>'CounterColumn',
		),
		array(
			'header'=>'Naslov',
			'name'=>'title',
		),
		array(
			'header'=>'URL',
			'name'=>'url',
			'type'=>'raw',
			'value'=>'CHtml::link($data->url, $data->url)',
		),
		array(
			'header'=>'Datum dodavanja',
			'name'=>'date_added',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{delete}',
			'buttons'=>array(
				'view'=>array(
					'label'=>'Pogledaj',
				),
				'delete'=>array(
				   'label'=>'Obriši',
				),
			)
		),
	),
));