<?php
/* @var $this SiteSearchController */
/* @var $model Site */

$this->breadcrumbs=array(
	'Stranice'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Uredi',
);

$this->menu=array(
	array('label'=>'Sve stranice', 'url'=>array('index')),
	array('label'=>'Dodaj novu stranicu', 'url'=>array('create')),
        array('label'=>'Pregledaj stranicu', 'url'=>array('view', 'id'=>$model->id)),
	array(
		'label'=>'Obriši stranicu', 'url'=>'#', 
		'linkOptions'=>
			array('submit'=>array('delete','id'=>$model->id),
				'confirm'=>'Jeste li sigurni da želite izbrisati stranicu?'
			)
	),
);
?>

<h1>Uredi <?php echo $model->title; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>