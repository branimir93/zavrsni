<?php
/* @var $this SiteSearchController */
/* @var $model Site */

$this->breadcrumbs=array(
	'Stranice'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Sve stranice', 'url'=>array('index')),
	array('label'=>'Dodaj novu stranicu', 'url'=>array('create')),
	array('label'=>'Uredi stranicu', 'url'=>array('update', 'id'=>$model->id)),
	array(
		'label'=>'Obriši stranicu', 'url'=>'#', 
		'linkOptions'=>
			array('submit'=>array('delete','id'=>$model->id),
				'confirm'=>'Jeste li sigurni da želite izbrisati stranicu?'
			)
	),
);
?>

<h1><?php echo $model->title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'url',
		'title',
		'date_added',
		'url_template',
		'form_type',
		'type'
	),
)); ?>

<br />
<?php 
if($model->form_type == 'POST'){
	echo '<h3>POST parametri</h3>';
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model->getSitePost(),
		'attributes'=>array(
			'post_fields',
			'boundary',
		),
	));
}?>
<br />
<h3>Pretrage</h3>
<?php 
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'querySite-grid',
		'dataProvider'=> $querySiteModel,
		'summaryText' => 'Prikazujem {start}-{end} od {count}',
		'emptyText' => 'Ne postoji ni jedna stranica.',
		'columns'=>array(
			array(
				'header'=>'#',
				'class'=>'CounterColumn',
			),
			array(
				'header'=>'Ključna riječ',
				'type'=>'raw',
				'htmlOptions'=>array(
					'width'=>'200',
				 ),
				'value'=>'$data->keyword->text',
			),
			array(
				'header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status->text',
			),
			array(
				'header'=>'Broj rezultata',
				'type'=>'raw',
				'htmlOptions'=>array(
					'width'=>'55',
				 ),
				'value'=>'$data->query_result_num',
			),
			array(
				'header'=>'Početak obrade',
				'type'=>'raw',
				'value'=>'$data->query_start',
			),
			array(
				'header'=>'Kraj obrade',
				'type'=>'raw',
				'value'=>'$data->query_finish',
			),
			array(
				'class'=>'CButtonColumn',
				'template'=>'{view}',
				'buttons'=>array(
					'view'=>array(
						'label'=>'Pogledaj',
						'url'=>'Yii::app()->createUrl("query/view", array("id"=>$data->query->id))',
					),
				)
			),
		),
	));
	?>