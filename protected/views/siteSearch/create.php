<?php
/* @var $this SiteSearchController */
/* @var $model Site */

$this->breadcrumbs=array(
	'Stranice'=>array('index'),
	'Dodaj novu stranicu',
);

$this->menu=array(
	array('label'=>'Sve stranice', 'url'=>array('index')),
);
?>

<h1>Dodaj novu stranicu</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>