<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'query-result-grid',
	'dataProvider'=>$result->search($model_id),
	'filter'=>$result,
	'summaryText' => 'Prikazujem {start}-{end} od {count}',
	'emptyText' => 'Ne postoji ni jedan rezultat.',
	'columns'=>array(
		array(
			'header'=>'#',
			'class'=>'CounterColumn',
		),
		array(
			'header'=>'Stranica',
			'name'=>'site_name',
			'type'=>'raw',
			'value'=>'CHtml::link($data->querySite->site->title, ' .
					"array('siteSearch/view', 'id'=>\$data->querySite->site->id));",
		),
		array(
			'header'=>'URL (rezultati)',
			'name'=>'url',
			'type'=>'raw',
			'value'=>'CHtml::link($data->url, $data->url)',
		),
	),
));