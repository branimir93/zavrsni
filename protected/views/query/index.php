<?php
/* @var $this QueryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Povijest',
);
$this->menu=array(
	array('label'=>'Nova pretraga', 'url'=>array('site/index')),
);
?>

<h1>Povijest</h1>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'guery-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => 'Prikazujem {start}-{end} od {count}',
        'emptyText' => 'Ne postoji ni jedan upit.',
	'columns'=>array(
		array(
			'header'=>'#',
			'class'=>'CounterColumn',
		),
		array(
			'header'=>'Pojam',
			'name'=>'keyword_text',
			'value'=>'$data->keyword->text',
		),
		array(
			'header'=>'Stranice',
			'name'=>'sites_titles',
			'class'=>'SitesColumn',
		),
		array(
			'header'=>'Datum',
			'name'=>'time',
			'value'=>'$data->time',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{delete}',
			'buttons'=>array(
				'view'=>array(
					'label'=>'Pogledaj',
				),
				'delete'=>array(
					'label'=>'Obriši',
				),
			)
		),
	),
));
