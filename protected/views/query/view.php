<?php
/* @var $this QueryController */
/* @var $model Query */
/* @var $result QueryResult */
/* @var $siteDataProvider CArrayDataProvider */

$this->breadcrumbs=array(
	'Povijest'=>array('index'),
	$model->keyword->text . ' (' . $model->time . ')',
);

$this->menu=array(
	array('label'=>'Povijest', 'url'=>array('index')),
	array(
		'label'=>'Izbriši', 'url'=>'#', 
		'linkOptions'=>
			array('submit'=>array('delete','id'=>$model->id),
				'confirm'=>'Jeste li sigurni da želite izbrisati ovu pretragu?'
			)
	),
);
?>

<h1>
	Pojam: <?php echo $model->keyword->text, ' (', $model->time, ')'; ?>
</h1>

<?php 

$this->renderPartial('_gridview_results', array(
  'result' => $result,
  'model_id' => $model_id,
));
?>
<div class="scrollable">
	<?php 
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'querySite-grid',
		'dataProvider'=> $siteDataProvider,
		'summaryText' => 'Prikazujem {start}-{end} od {count}',
		'emptyText' => 'Ne postoji ni jedna stranica.',
		'enablePagination'=>false,
		'columns'=>array(
			array(
				'header'=>'#',
				'class'=>'CounterColumn',
			),
			array(
				'header'=>'Naslov',
				'type'=>'raw',
				'htmlOptions'=>array(
					'width'=>'220',
				 ),
				'value'=>'CHtml::link($data->site->title, ' .
					"array('siteSearch/view', 'id'=>\$data->site->id));",
			),
			array(
				'header'=>'Status',
				'type'=>'raw',
				'value'=>function($data) { 
					return '<div id="querysite'.$data->id.'_status">'.$data->status->text.'</div>'; 
				},
			),
			array(
				'header'=>'Broj rezultata',
				'type'=>'raw',
				'htmlOptions'=>array(
					'width'=>'55',
				 ),
				'value'=>function($data) { 
					return '<div id="querysite'.$data->id.'_result_num">'.$data->query_result_num.'</div>'; 
				},
			),
			array(
				'header'=>'Početak obrade',
				'type'=>'raw',
				'value'=>function($data) { 
					return '<div id="querysite'.$data->id.'_query_start">'.$data->query_start.'</div>'; 
				},
			),
			array(
				'header'=>'Kraj obrade',
				'type'=>'raw',
				'value'=>function($data) { 
					return '<div id="querysite'.$data->id.'_query_finish">'.$data->query_finish.'</div>'; 
				},
			),
		),
	));
	?>
</div>

	<?php 

$za_proci = [];
$gotovo = [];
foreach($model->querySites as $qs){
	if($qs->status_id == 1){
		$za_proci[$qs->id] = $qs->site_id;
	}
	else {
		$gotovo[$qs->id] = $qs->site_id;
	}	
}
$ubaci = 'var za_proci = '.CJSON::encode($za_proci). '; var gotovo = '.
		CJSON::encode($gotovo). ';';
	
$cs = Yii::app()->getClientScript();
$cs->registerScript('button', $ubaci .
<<<JSKOD
var trenutno = [];
var i = 0;

var idiDalje = function(){
	if(Object.keys(trenutno).length > 20){
		setTimeout(idiDalje, 500);
	} else {
		if(Object.keys(za_proci).length > 0){
			for(var a in za_proci){
				trenutno[a] = za_proci[a];
				getData(a, za_proci[a]);
				delete za_proci[a];
				idiDalje();
				break;
			}
		}
	}
	
	if(i>10){
		refreshGrid();
		i = 0;
	}
}

var getData = function(idQuery) {
	var status = '';
	var now = new Date();
	$('#querysite' + idQuery + '_status').html('OBRAĐUJEM');
	$('#querysite' + idQuery + '_query_start').html(now.toISOString());
	$.ajax({
		url: '/zavrsni/index.php/query/getLinks/' + idQuery,
		type: 'GET',
		dataType: 'json',
	})
	.fail(function( jqXHR, textStatus, errorThrown ){
		$('#querysite' + idQuery + '_status').html('S_ERROR');
    })	
    .done(function( data, textStatus, jqXHR ){
		$('#querysite' + idQuery + '_status').html(data.status);
		$('#querysite' + idQuery + '_result_num').html(data.result_num);
		$('#querysite' + idQuery + '_query_start').html(data.query_start);
		$('#querysite' + idQuery + '_query_finish').html(data.query_finish);
    })
	.always(function(){
		gotovo[idQuery] = trenutno[idQuery];
		delete trenutno[idQuery];
		i++;
		if(Object.keys(trenutno).length == 0 &&
			Object.keys(trenutno).length == 0){
			refreshGrid();
		}
    });
};

var refreshGrid = function(){
		$.fn.yiiGridView.update('query-result-grid');
}

idiDalje();
JSKOD
);

