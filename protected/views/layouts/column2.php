<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php $this->widget('Flashes'); ?>
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Izbornik',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'sideMenu'),
		));
		$this->endWidget();
	?>
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>'Zadnje pretrage',
		));
		$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->recentQueries,
				'htmlOptions'=>array('class'=>'recentQueries'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>