<?php
/* @var $this SiteController */
/* @var $model Query */
/* @var $smodel Site[] */
/* @var $recentQueries */

$this->pageTitle=Yii::app()->name;
$this->breadcrumbs=array(
	'Pretraga'
);
?>

<h1>Pretraga</h1>

<div class="form">
        
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>CHtml::normalizeUrl(array('query/create')),
	'id'=>'query-form',
)); ?>
	
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->label($model,'keyword'); ?>
		<?php echo $form->textField($model,'keyword', array('size'=>'100')); ?>
	</div>
	
	<div class="row">
		<div class="scrollable">
		<?php 
		$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'site-grid',
			'dataProvider'=>$smodel->search(),
			'enablePagination'=>false,
			'summaryText' => 'Prikazujem {start}-{end} od {count}',
			'emptyText' => 'Ne postoji ni jedna stranica.',
			'columns'=>array(
				array(
					'header'=>'#',
					'class'=>'CounterColumn',
					'htmlOptions'=>array(
						'style'=>'width: 20px;'
					 ),
				),
				array(
					'class'=>'CCheckBoxColumn',
					'selectableRows'=>2,
					'checkBoxHtmlOptions'=>['name'=>'Query[sites][]'],
				),
				array(
					'header'=>'Naslov',
					'type'=>'raw',
					'value'=>function($data) { 
						return '<div width="100">'.CHtml::link(substr($data->title,0, 40),
								array('siteSearch/view', 'id'=>$data->id)).'</div>'; 
					},
				),
				array(
					'header'=>'URL',
					'name'=>'url',
					'type'=>'raw',
					'value'=>function($data) { 
						return '<div width="100">'.CHtml::link(substr($data->url,0, 40), $data->url).'</div>'; 
					},
				),
				array(
					'header'=>'Datum dodavanja',
					'name'=>'date_added',
					'htmlOptions'=>array(
						'style'=>'width: 120px;'
					 ),
				),
			),
		));
		?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Pretraži'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>